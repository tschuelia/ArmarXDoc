/**

\page ArmarXDoc-Dependecies-Installation ArmarX Dependencies - Installation from source

We recommend to use the H2T package server to install the prerequisites that are needed for ArmarX as described in \ref ArmarXDoc-Dependecies-Installation-custom-deb "ArmarX Dependencies - Package Installation".
Nevertheless, installation from source is possible as shown below.

\note If you are working on a H2T lab pc at KIT, all dependencies are already installed on your system!

\section ArmarXDoc-Dependecies-Installation-Manual-standard-packages ArmarX Dependencies - Standard Ubuntu Packages

First, ensure that the list of standard Ubuntu packages are installed on your system as described here:\n
\ref ArmarXDoc-Dependecies-Installation-ubuntu-deb

\section ArmarXDoc-Dependecies-Installation-Manual ArmarX Custom Dependencies - Installation from source

Several ArmarX packages introduce dependencies to custom packages. In the following, the installation of these packages from source is described.

\subsection ArmarXDoc-Dependecies-Installation-Manual-IVT Building IVT from source

The Integrating Vision Toolkit (IVT) is used by the VisionX package. Download and build it with the following commands:

\verbatim
cd ~
git clone https://i61wiki.itec.uka.de/git/ivt.git IVT
cd IVT
mkdir build
cd build
cmake ..
make -j4
# optional:
sudo make install
\endverbatim

\note The packages *ivtrecognition* and *ivtarmar3* are currently only provided as binary packages from the package server. Some VisionX components will be disabled without these packages.


\subsection ArmarXDoc-Installation-dependencies-build-bullet Building Bullet from source

We recommend using the *simox* deb-package from our package server which automatically installs the appropriate bullet package.
Nevertheless, the manual installation instructions are given below.

Pick the 2.82/2.83 source code archive (e.g. *bullet-2.82-r2704.tgz* or *2.83.7.tar.gz*) and uncompress it to a folder on your PC.
Then, go to the build directory, trigger cmake and compile the sources:

\verbatim
wget https://github.com/bulletphysics/bullet3/archive/2.83.7.tar.gz
tar xf 2.83.7.tar.gz
cd bullet3-2.83.7
mkdir build
cd build
cmake .. -DBUILD_SHARED_LIBS=on -DCMAKE_BUILD_TYPE=Release -DUSE_DOUBLE_PRECISION=on
make -j4
\endverbatim

\subsection ArmarXDoc-Installation-dependencies-build-simox Building Simox from source with Bullet support

In order to compile simox with bullet support, please first compile the bullet package as described above.
We recommend using the *simox* deb-package from our package server which also automatically installs the appropriate bullet package.

The simox sources can be get from the git repository:

\verbatim
git clone https://gitlab.com/Simox/simox.git
\endverbatim

Configure simox (you might need to adjust the path to bullet!)

\verbatim
cd simox
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DSimox_BUILD_SimDynamics=on -DSimDynamics_USE_BULLET_DOUBLE_PRECISION=on -DBULLET_ROOT="../../bullet3-2.83.7/build" -DBULLET_INCLUDE_DIR="../../bullet3-2.83.7/src" 
\endverbatim

Compile the sources with:

\verbatim
make -j4
\endverbatim

Additional information can be found at the simox wiki pages: https://gitlab.com/Simox/simox/wikis/home


\subsection ArmarXDoc-Installation-dependencies-build-mmm Building MMM from source

This step is only necessary if you need MMM in any way. This might be the case if you want to play back trajectories given in the MMM data format.

The project's webpage is https://gitlab.com/mastermotormap/mmmcore.

\verbatim
git clone https://gitlab.com/mastermotormap/mmmcore.git MMMCore
cd MMMCore
mkdir build
cd build
cmake ..
make -j4
\endverbatim

If you also need the MMMTools package you can compile it as follows:

\verbatim
git clone https://gitlab.com/mastermotormap/mmmtools.git MMMTools
cd MMMTools
mkdir build
cd build
cmake ..
make -j4
\endverbatim



\subsection ArmarXDoc-Installation-dependencies-build-pcl Building PCL from source

This step is only necessary if you require PCL version 1.8.
Additional information can be found at the PCL page http://pointclouds.org/

\verbatim
git clone --depth 1 -b pcl-1.8.0rc2 https://github.com/PointCloudLibrary/pcl
cd pcl
mkdir build
cd build
cmake ..
make -j4
\endverbatim


*/
